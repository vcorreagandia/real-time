// I have used this solution to let the io object cached. So it means wherever you want to call io usin require(...)
// You will get precached object
let io;
module.exports = {
  init: function(server) {
    // start socket.io server and cache io value
    const { Server } = require("socket.io");
    io = new Server(server);
    _initServices();
    return io;
  },
  getio: function() {
    // return previously cached value
    if (!io) {
      throw new Error("must call .init(server) before you can call .getio()");
    }
    return io;
  }
}

// Private function we dont want to export
function _initServices() {
  io.on("connection", (socket) => {
    // Notifies when a new user joins the call and sends the information to all the users that are in this socket
    socket.on('newUser', (id, room) => {
      socket.join(room);
      socket.to(room).broadcast.emit('userJoined' , id);
      socket.on('disconnect' , () => {
          socket.to(room).broadcast.emit('userDisconnect' , id);
      })
    })

    // Notifies when a user is drawing.
    socket.on('draw', (data, userId) => {
      io.emit('draw', data, userId);
    });
  
  })
}
