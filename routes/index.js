const router = require('express').Router();
const { v4: uuidv4 } = require("uuid");

router.get('/', (req,res, next)=>{
  res.redirect(`/${uuidv4()}`);
});

router.get("/:id", (req, res, next) => {
  res.render("index", { id: req.params.id })
});

module.exports = router;
