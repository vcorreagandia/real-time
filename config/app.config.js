
const express = require('express');
const routes = require('../routes/index');
const app = express();

app.set("view engine", "ejs");
app.use(express.static('public'));


// Settings
app.set('port', process.env.PORT || 3000);

// Routes
app.use('/', routes);

module.exports = app;
