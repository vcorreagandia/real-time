const socket = io('/');
const peer = new Peer();
const videoGrid = document.getElementById('video-container');
const mainVideo = document.createElement('video');
const drawButton = document.getElementById('draw');
mainVideo.muted = true;
const peerConnections = {};
let myVideoStream, myId, canvas, ctx, coord = { x: 0, y: 0 };

drawButton.addEventListener('click', () => {
  createCanvas(0,0)
});
document.addEventListener("mousedown", start);
document.addEventListener("mouseup", stop);

navigator.mediaDevices.getUserMedia({
  video:true,
  audio:true
}).then((stream)=>{
  myVideoStream = stream;
  addVideo(mainVideo , stream);
  peer.on('call' , call => {
    call.answer(stream);
    const incomingCall = document.createElement('video');
    incomingCall.setAttribute('id', call.peer)
    call.on('stream', userStream=>{
      addVideo(incomingCall , userStream);
    })
    call.on('error', (err)=>{
      alert(err)
    })
    call.on("close", () => {
        incomingCall.remove();
    })
    peerConnections[call.peer] = call;

  })
}).catch(err=>{
    alert(err.message)
})

peer.on('open', (id) => {
  myId = id;
  mainVideo.setAttribute('id', myId)

  // emit an event when a new user join in
  socket.emit("newUser", id, roomId);
})

// throw an error if peer fails
peer.on('error', (err)=>{
  alert(err.type);
});

// received info regarding "userJoined" event
socket.on('userJoined', id => {

  const call  = peer.call(id, myVideoStream);
  const incomingCall = document.createElement('video');
  incomingCall.setAttribute('id', id)
  call.on('error', (err)=>{
    alert(err);
  })
  call.answer(myVideoStream)
  call.on('stream', userStream=>{
    addVideo(incomingCall , userStream);
  })
  call.on('close', ()=>{
    incomingCall.remove();
  })
  peerConnections[id] = call;

})

// received info regarding "userDisconnect" event
socket.on('userDisconnect', id=>{
  // we need to close the peer connection if the user has disconnected
  if (peerConnections[id]) {
    peerConnections[id].close();
  }
})

// received info regarding "draw"
socket.on('draw', (data, userId) => {
  // Verify if we are sending the info 
  if (userId != myId) {
    // Init canvas once
    if (!canvas) {
      const videoPosition = document.getElementById(userId).getBoundingClientRect();
      createCanvas(videoPosition.x, videoPosition.y);
    }
    reposition(data)
    draw(data, userId)
  }
})

function addVideo(video, stream){
  video.srcObject = stream;
  video.addEventListener('loadedmetadata', () => {
    video.play()
  })
  videoGrid.append(video);
}

function start(event) {
  // track the mousemove event only if we have a canvas
  if (ctx) {
    document.addEventListener("mousemove", draw);
    reposition(event);
  }
}

function reposition(event) {
  coord.x = event.x;
  coord.y = event.y;
}

function stop() {
  document.removeEventListener("mousemove", draw);
}

function draw(event, id) {
  // Prevent to draw when the event comes from the server
  if (!id) {
    socket.emit('draw', coord, myId)
  } 
  ctx.beginPath();
  ctx.lineWidth = 5;
  ctx.lineCap = "round";
  ctx.strokeStyle = "#ACD3ED";
  ctx.moveTo(coord.x, coord.y);
  reposition(event);
  ctx.lineTo(coord.x, coord.y);
  ctx.stroke();
}

function createCanvas(x, y) {
  canvas = document.createElement("canvas");
  canvas.width = 300;
  canvas.height = 300;
  canvas.style.top = `${y}px`;
  canvas.style.left = `${x}px`;
  ctx = canvas.getContext("2d");
  videoGrid.append(canvas);
}