# real-time

## About
    Basic project in which you can simulate a video call, and paint on the screen of your interlocutor who will be able to see it.
    Nodejs, WebRTC and SocketIO Technologies
    Design ideas have been taken which may be debatable because the project has been done in 1 day due to time constraints.
    To use the multi-call share the url.

## Setps

    1. Install dependencies
    2. Run the project 
    ```
    npm i 
    npm run dev
    
    ```
