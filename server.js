
const app = require('./config/app.config');
const http = require('http');
const server = http.createServer(app);
const { ExpressPeerServer } = require('peer');
const peer = ExpressPeerServer(server , {
  debug:true
});
const io = require('./services/socket-io').init(server);

server.listen(app.get('port'));

server.on('error', (err) => {
  console.error(err);
});

server.on('listening', async () => {
  console.log(`Listening on port ${app.get('port')}`);
});
